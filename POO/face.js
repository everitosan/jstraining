function Face(x, y) {
    this.face = 80
    this.eye = 30
    this.mouth = 15
    this.y = y
    this.x = x
}


Face.prototype.draw = function( ) {
    ellipse(this.x, this.y, this.face, this.face)
    ellipse(this.x+20, this.y-10, this.eye, this.eye)
    ellipse(this.x-20, this.y-10, this.eye, this.eye)
    line(this.x-15, this.y+25, this.x+15, this.y+25)
}