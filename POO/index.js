const faces = []
const nFaces = 2

function setup() {
    createCanvas(windowWidth, windowHeight-10);

    for(let i = 0; i < nFaces; i++) {
        const tmpFace = new Face((100*(i+1)), 50);
        faces.push(tmpFace);
    }
}

function draw() {
    faces.forEach(face => face.draw());
}